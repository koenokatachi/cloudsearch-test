#!/bin/bash

#~
#~ Tested on ubuntu 14.04.
#~ Dependencies:
#~ 	curl
#~ 	either js-beautify or jq
#~

DATE_FMT='%Y-%m-%dT%T%Z'

function bye(){
	echo $@
	exit 1
}

function q1(){
	"$@" >/dev/null
}

function q2(){
	"$@" 2>/dev/null
}

function utc_date(){
	date -u +$DATE_FMT -d @$1
}

function fetch(){
	status $1 ...
	q2 curl -A funabori-funabori-funabori --compress -H 'Cookie: over18=1' "https://www.reddit.com$2"
	status
}
function search(){
	#~ sort=new means the first result should be closest to $2
	fetch search "/r/all/search.json?syntax=cloudsearch&q=timestamp:${1}..${2}&limit=10&sort=new"
}

function status() {
	echo -ne "\r\033[K"$@ >&2
}

#~ ===================== Check setup and input =========================

q1 which curl || bye curl is not found

if q1 which js-beautify; then
	FORMAT_JSON_CMD=js-beautify
elif q1 which jq; then
	FORMAT_JSON_CMD='jq -M .'
fi

[[ -n "$FORMAT_JSON_CMD" ]] || bye Neither js-beautify nor jq are found

edge="$1"
[[ -n "$edge" ]] || bye Usage: $(basename $0) '<YYYY-MM-DD> [<TZ offset in hours>]'

edge=$(q2 date -u -d "$edge" +%s) || bye Wrong date format

TZ_OFFSET="${2:-0}"
((TZ_OFFSET+=0))

#~ ===================== Processing ====================================

echo Edge = $(utc_date $edge)
echo TZ offset = ${TZ_OFFSET}h

((TZ_OFFSET*=3600))

#~ We gonna query for a timestamp range L..R.
#~ Ranges ..R don't work past ~2015-09-01 that's why L..R.
L=$(($edge-86400))
R=$(($edge-1))

echo L = $(utc_date $L)
echo R = $(utc_date $R)

echo ========== Results ==============
search $((L + TZ_OFFSET)) $((R + TZ_OFFSET)) |\
	$FORMAT_JSON_CMD |\
	grep -P '"(name|created_utc)"' |\
	tr -d '",' |\
	paste - - |\
	awk '{if ($2 ~ /^t3_/) { id=$2; ts=$4 } else { id=$4; ts=$2 } print id "\t" strftime("'$DATE_FMT'", ts, 1)}'
echo =================================
