It is all about reddit API.

I'm looking for minimal post ID past (or exactly at) some timestamp `T`, e.g. "which post ID was first since 2015-01-01 00:00:00 UTC?".

Lists don't expose all posts (e.g. posts by removed users are not listed) so I use a combination of
- `/r/all/search.json?syntax=cloudsearch&q=timestamp:L..R&sort=new` (`R=T-1`, `L=T-1day`) API call to get a very close __lower__ edge
- `/api/info.json` API call for 100 posts past the edge and pick the one required from the result set.

It would work if only the cloudsearch thing worked as expected.

__Problem1__: `timestamp` parameter is not stable in terms of TZ on the server side. For some dates it is treated as UTC, for others it is UTC-8:

_Post1 is the top post in the result set by the cloudsearch API call_

|Date|Post1 ID|Post1 created_utc|
|:---|:---:|---:|
|2016-01-01|t3_3yx791|2015-12-31T15:59:59GMT|
|2011-07-01|t3_idqne|2011-06-30T23:59:49GMT|

It is not a big problem on its own: we can automaticly adjust `L` and `R` and issue another API call.

The script only issues a single API call but TZ can be adjusted via command line.

The script accepts 2 arguments:
- YYYY-MM-DD date (otherwise it can be anything `date` could parse). It is `T`
- TZ offset in hours. It can be used to adjust `L` and `R` in the cloudsearch API call to match the TZ settings on the server side.

So for 2016-01-01 we just run it as `./cloudsearch-test.sh 2016-01-01 8` and the top post is 2015-12-31T23:59:58GMT.

Now to the rly bad stuff
-------------------

__Problem2__: sometimes the result set is not usable.

First let's see how it is supposed to work.

In the output below notice:
- posts are sorted by timestamps desc
- posts within the same timestamp have no strict order. Look at the 3 posts by 2015-12-31T23:59:55GMT. IDs are (in the same sequence): X, X-2, X-1.

I don't care about sorting within same timestamp groups since I only need __some__ lower edge, not the strict one.

```
$ ./cloudsearch-test.sh 2016-01-01 8
Edge = 2016-01-01T00:00:00UTC
TZ offset = 8h
L = 2015-12-31T00:00:00UTC
R = 2015-12-31T23:59:59UTC
========== Results ==============
t3_3yyumz	2015-12-31T23:59:58GMT
t3_3yyun0	2015-12-31T23:59:58GMT
t3_3yyumw	2015-12-31T23:59:56GMT
t3_3yyumv	2015-12-31T23:59:55GMT
t3_3yyumt	2015-12-31T23:59:55GMT
t3_3yyumu	2015-12-31T23:59:55GMT
t3_3yyumq	2015-12-31T23:59:54GMT
t3_3yyums	2015-12-31T23:59:54GMT
t3_3yyumo	2015-12-31T23:59:53GMT
t3_3yyumj	2015-12-31T23:59:52GMT
=================================
```

Now let's try 2012-07-01.

```
$ ./cloudsearch-test.sh 2012-07-01
Edge = 2012-07-01T00:00:00UTC
TZ offset = 0h
L = 2012-06-30T00:00:00UTC
R = 2012-06-30T23:59:59UTC
========== Results ==============
t3_vuetx	2012-06-30T15:59:50GMT
t3_vv0ye	2012-06-30T23:59:48GMT
t3_vv0yd	2012-06-30T23:59:45GMT
t3_vuhhn	2012-06-30T16:59:41GMT
t3_vv0y9	2012-06-30T23:59:40GMT
t3_vuhhm	2012-06-30T16:59:37GMT
t3_vuhhk	2012-06-30T16:59:36GMT
t3_vuete	2012-06-30T15:59:33GMT
t3_vuhhj	2012-06-30T16:59:33GMT
t3_vuhhh	2012-06-30T16:59:31GMT
=================================
```

cloudsearch thinks the posts are consecutive while they are definitely not both in terms of `created_utc` and post IDs.

The first post is of 2012-06-30T15:59:50GMT. Let's try TZ offset 8:

```
./cloudsearch-test.sh 2012-07-01 8
Edge = 2012-07-01T00:00:00UTC
TZ offset = 8h
L = 2012-06-30T00:00:00UTC
R = 2012-06-30T23:59:59UTC
========== Results ==============
t3_vvfae	2012-07-01T07:59:52GMT
t3_vvfab	2012-07-01T07:59:50GMT
t3_vvfa6	2012-07-01T07:59:47GMT
t3_vvfa4	2012-07-01T07:59:45GMT
t3_vvfa2	2012-07-01T07:59:40GMT
t3_vvfa1	2012-07-01T07:59:38GMT
t3_vvf9z	2012-07-01T07:59:32GMT
t3_vv0xj	2012-06-30T23:59:18GMT
t3_vvf9u	2012-07-01T07:59:15GMT
t3_vv0xb	2012-06-30T23:59:12GMT
=================================
```

Same wrong ordering.

Such results are still usable. Any 23:59:XX post would work and there are some in the sample results sets. But it is still not how the API call is supposed to work.
